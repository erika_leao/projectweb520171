Projeto WEB5  Particular
Product Backlog
Prepara inicialmente a aplicação  0/5
Como usuário admin do preciso ter acesso ao sistema para gerencia­lo  0/8
Como usuário admin posso cadastrar os usuários comuns  0/3
Como usuário admin posso cadastrar as categorias de solicitação para atendimento  0/3
Como usuário admin posso gerenciar as instruções educacionais que irão utilizar o sistema  0/3
Como usuário admin do sistema posso cadastrar uma solicitação de atendimento  0/5
Como usuário do sistema preciso realizar login para solicitação de atendimento  0/8
Como usuário do sistema posso solicitar uma ordem de serviço, sendo que cada ordem de serviço pode conter diversos serviços  0/5
Como usuário admin do sistema posso verifica as ordens de serviço e altera o status (pré­agendado, em andamento, concluído). A cada alteração realizada, o usuário que realizou a ordem de serviço é notificado via sistema e email.  0/4
Como usuário admin posso comentar a solicitação de atendimento para mais informações e aguardar a notificação da reposta de usuário  0/3
O sistema deve registar o usuário admin alterou a solicitação de atendimento  0/1